# Terminal user interface tolls (tuitools)

Conjunto de ferramentas criadas para o meu uso pessoal no terminal.

## Dependências

A maioria desses scripts requer:

* Bash (>=5)
* FuzzyFinder (fzf)
* Gawk

## Configurações

As definições de opções pessoais, quando existem, são feitas nos próprios scripts.


## TODO

* Incluir licença em todos os scripts.
* Incluir teste e criação automática de arquivos de configuração.
* Substituir o `fzf` por menus do tipo `while/read` onde possível.

Valeu FC e PK! Funcionou!
